// async 脚本虽然马上加载，但是并不马上执行，所以，浏览器解析到这一句代码时，还没有执行 jquery 库。
console.log(typeof $ === 'undefined');
console.log(typeof angular !== 'undefined');